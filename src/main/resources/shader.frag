#version 330

in vec4 pos;
in vec4 previousPos;
in vec2 texCoord;

uniform sampler2D surfaceTexture;

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 motionVector;

void main() {
    color = texture(surfaceTexture, texCoord);

    vec3 NDCPos = (pos / pos.w).xyz;
    vec3 prevNDCPos = (previousPos / previousPos.w).xyz;
    motionVector = vec4((NDCPos - prevNDCPos).xyz, 0.0);
}