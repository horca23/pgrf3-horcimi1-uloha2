#version 330

in vec2 inPosition;

uniform mat4 MVPMatrix;
uniform mat4 previousMVPMatrix;

out vec4 pos;
out vec4 previousPos;
out vec2 texCoord;

void main() {
    texCoord = inPosition; // grid is [0, 0] - [1, 1] -> vertex position can be used as texture coordinations

    pos = MVPMatrix * vec4(inPosition, 0.0, 1.0);
    previousPos = previousMVPMatrix * vec4(inPosition, 0.0, 1.0);

    gl_Position = pos;
}