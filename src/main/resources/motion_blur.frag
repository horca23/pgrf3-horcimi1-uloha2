#version 330

in vec2 texCoord;

uniform int motionBlurSwitch;

uniform sampler2D colorTexture;
uniform sampler2D motionTexture;

out vec4 fragColor;

const int numSamples = 20;

void main() {

    if (motionBlurSwitch == 0) {
        fragColor = texture(colorTexture, texCoord);
    } else {
        vec2 UV = texCoord;
        vec2 motionVector = texture(motionTexture, UV).xy / 15.0;
        vec4 color = texture(colorTexture, UV);

        UV += motionVector;
        for (int i = 1; i < numSamples; ++i, UV += motionVector) {
            vec4 currentColor = texture(colorTexture, UV);
            color += currentColor;
        }

        fragColor = color / numSamples;

    }

}