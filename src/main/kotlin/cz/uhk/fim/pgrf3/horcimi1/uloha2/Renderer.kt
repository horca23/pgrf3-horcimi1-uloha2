package cz.uhk.fim.pgrf3.horcimi1.uloha2

import com.jogamp.newt.event.KeyEvent
import com.jogamp.newt.event.KeyListener
import com.jogamp.newt.event.MouseEvent
import com.jogamp.newt.event.MouseListener
import com.jogamp.opengl.GL2
import com.jogamp.opengl.GL2GL3
import com.jogamp.opengl.GLAutoDrawable
import com.jogamp.opengl.GLEventListener
import oglutils.*
import transforms.*

const val CAM_SPEED = .01

class Renderer(
        private var width: Int,
        private var height: Int
) : GLEventListener, MouseListener, KeyListener {

    private var motionBlur = true
    private var rotate = true

    private lateinit var buffers: OGLBuffers
    private lateinit var texture: OGLTexture2D
    private lateinit var renderTarget: OGLRenderTarget
    private lateinit var colorTexture: OGLTexture2D
    private lateinit var motionVectorTexture: OGLTexture2D
    private lateinit var textureViewer: OGLTexture2D.Viewer
    private lateinit var textRenderer: OGLTextRenderer

    private var projectionMatrix = getProjectionMatrix()
    private var camera = Camera()
            .withPosition(Vec3D(1.0, 1.0, 0.25))
            .withAzimuth(Math.PI * 1.25)
            .withZenith(Math.PI * -.125)
            .withFirstPerson(true)
    private var mx = 0
    private var my = 0

    private var previousMVPMatrix = camera.viewMatrix.mul(projectionMatrix)

    private var shaderProgram = 0
    private var locMVPMatrix = 0
    private var locPreviousMVPMatrix = 0

    private var motionBlurShaderProgram = 0
    private var locMotionBlurSwitch = 0

    private var time = 0.0
    private var timeIncrement = 0.03


    override fun init(drawable: GLAutoDrawable) {
        val gl = drawable.gl.gL2GL3
        println("Init GL is ${gl.javaClass.name}")
        println("OpenGL version ${gl.glGetString(GL2.GL_VERSION)}")
        println("OpenGL vendor ${gl.glGetString(GL2.GL_VENDOR)}")
        println("OpenGL renderer ${gl.glGetString(GL2.GL_RENDERER)}")
        println("OpenGL extension ${gl.glGetString(GL2.GL_EXTENSIONS)}")
        OGLUtils.shaderCheck(gl)

        shaderProgram = ShaderUtils.loadProgram(gl, "/shader.vert", "/shader.frag", null, null, null, null)
        locMVPMatrix = gl.glGetUniformLocation(shaderProgram, "MVPMatrix")
        locPreviousMVPMatrix = gl.glGetUniformLocation(shaderProgram, "previousMVPMatrix")

        motionBlurShaderProgram = ShaderUtils.loadProgram(gl, "/motion_blur.vert", "/motion_blur.frag", null, null, null, null)
        locMotionBlurSwitch = gl.glGetUniformLocation(motionBlurShaderProgram, "motionBlurSwitch")

        createBuffers(gl)
        texture = OGLTexture2D(gl, "/texture.jpg")
        renderTarget = getRenderTarget(gl)

        textureViewer = OGLTexture2D.Viewer(gl)

        textRenderer = OGLTextRenderer(gl, width, height)
    }

    override fun display(drawable: GLAutoDrawable) {
        val gl = drawable.gl.gL2GL3

        renderPass(gl)
        motionBlurPass(gl)

        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL)
        textureViewer.view(colorTexture, -1.0, 0.5, 0.5)
        textureViewer.view(motionVectorTexture, -1.0, 0.0, 0.5)

        textRenderer.drawStr2D(25, 100, "[M] Motion blur: $motionBlur")
        textRenderer.drawStr2D(25, 75, "[Up|Down] Rotation speed")
        textRenderer.drawStr2D(25, 50, "[Space] Rotate: $rotate")
        textRenderer.drawStr2D(25, 25, "[WASD|Shift|Control|Mouse] Camera")
    }

    private fun renderPass(gl: GL2GL3) {
        if (rotate) time += timeIncrement
        gl.glUseProgram(shaderProgram)

        renderTarget.bind()

        gl.glClearColor(0f, 0f, 0f, 1.0f)
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT or GL2GL3.GL_DEPTH_BUFFER_BIT)

        val currentMVPMatrix = Mat4Identity()
                .withRotation() * camera.viewMatrix * projectionMatrix
        gl.glUniformMatrix4fv(locMVPMatrix, 1, false, currentMVPMatrix.floatArray(), 0)
        gl.glUniformMatrix4fv(locPreviousMVPMatrix, 1, false, previousMVPMatrix.floatArray(), 0)

        texture.bind(shaderProgram, "surfaceTexture", 0)
        buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, shaderProgram)

        colorTexture = renderTarget.getColorTexture(0)
        motionVectorTexture = renderTarget.getColorTexture(1)

        previousMVPMatrix = currentMVPMatrix

    }

    private fun motionBlurPass(gl: GL2GL3) {
        gl.glUseProgram(motionBlurShaderProgram)

        gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0)
        gl.glViewport(0, 0, width, height)

        gl.glClearColor(0f, 0f, 0f, 1.0f)
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT or GL2GL3.GL_DEPTH_BUFFER_BIT)
        gl.glUniform1i(locMotionBlurSwitch, motionBlur.toInt())

        colorTexture.bind(motionBlurShaderProgram, "colorTexture", 0)
        motionVectorTexture.bind(motionBlurShaderProgram, "motionTexture", 1)

        buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, motionBlurShaderProgram)
    }

    override fun reshape(drawable: GLAutoDrawable, x: Int, y: Int, width: Int, height: Int) {
        this.width = width
        this.height = height
        projectionMatrix = getProjectionMatrix()
        renderTarget.deleteFramebuffers()
        renderTarget = getRenderTarget(drawable.gl.gL2GL3)
    }

    override fun dispose(drawable: GLAutoDrawable) {
        val gl = drawable.gl.gL2GL3
        gl.glDeleteProgram(shaderProgram)
        gl.glDeleteProgram(motionBlurShaderProgram)
        texture.deleteTexture()
        renderTarget.deleteFramebuffers()
    }

    override fun mouseDragged(e: MouseEvent) {
        camera = camera.addAzimuth(Math.PI * (mx - e.x) / width.toDouble()).addZenith(-Math.PI * (e.y - my) / height.toDouble())
        mx = e.x
        my = e.y
    }

    override fun mousePressed(e: MouseEvent) {
        mx = e.x
        my = e.y
    }

    override fun keyPressed(e: KeyEvent) {
        when (e.keyCode) {
            KeyEvent.VK_W -> camera = camera.forward(CAM_SPEED)
            KeyEvent.VK_S -> camera = camera.backward(CAM_SPEED)
            KeyEvent.VK_A -> camera = camera.left(CAM_SPEED)
            KeyEvent.VK_D -> camera = camera.right(CAM_SPEED)
            KeyEvent.VK_SHIFT -> camera = camera.up(CAM_SPEED)
            KeyEvent.VK_CONTROL -> camera = camera.down(CAM_SPEED)
            KeyEvent.VK_SPACE -> rotate = !rotate
            KeyEvent.VK_M -> motionBlur = !motionBlur
            KeyEvent.VK_UP -> timeIncrement = Math.min(timeIncrement + 0.01, 0.1)
            KeyEvent.VK_DOWN -> timeIncrement = Math.max(timeIncrement - 0.01, -0.1)
        }
    }

    private fun getProjectionMatrix() = Mat4PerspRH(Math.PI / 4, height / width.toDouble(), .01, 100.0)

    private fun getRenderTarget(gl: GL2GL3) = OGLRenderTarget(gl, width, height, 2)

    private fun createBuffers(gl: GL2GL3) {
        val grid = generateGridUsingTriangleStrip(2, 2)
        buffers = OGLBuffers(
                gl,
                grid.vertexBuffer.toFloatArray(),
                arrayOf(OGLBuffers.Attrib("inPosition", 2)),
                grid.indexBuffer.toIntArray()
        )
    }

    private operator fun Mat4.times(other: Mat4) = this.mul(other)

    private fun Mat4.withRotation(): Mat4 {
        return this * Mat4Transl(-0.5, -0.5, 0.0) * Mat4RotZ(time) * Mat4Transl(0.5, 0.5, 0.0)
    }

    private fun Boolean.toInt() = if (this) 1 else 0

    override fun keyReleased(e: KeyEvent) { /*noop*/ }

    override fun mouseReleased(e: MouseEvent) { /*noop*/ }

    override fun mouseMoved(e: MouseEvent) { /*noop*/ }

    override fun mouseEntered(e: MouseEvent) { /*noop*/ }

    override fun mouseWheelMoved(e: MouseEvent) { /*noop*/ }

    override fun mouseClicked(e: MouseEvent) { /*noop*/ }

    override fun mouseExited(e: MouseEvent) { /*noop*/ }

}