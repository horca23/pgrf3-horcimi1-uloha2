package cz.uhk.fim.pgrf3.horcimi1.uloha2

import com.jogamp.newt.event.WindowAdapter
import com.jogamp.newt.event.WindowEvent
import com.jogamp.newt.opengl.GLWindow
import com.jogamp.opengl.GLCapabilities
import com.jogamp.opengl.GLProfile
import com.jogamp.opengl.util.FPSAnimator
import kotlin.concurrent.thread

const val INITIAL_WIDTH = 1366
const val INITIAL_HEIGHT = 768
const val FPS = 60

fun main() {
    val glProfile = GLProfile.getGL2GL3()
    val glCapabilities = GLCapabilities(glProfile)

    val window = GLWindow.create(glCapabilities)
    val ren = Renderer(INITIAL_WIDTH, INITIAL_HEIGHT)
    window.addGLEventListener(ren)
    window.addMouseListener(ren)
    window.addKeyListener(ren)
    window.setSize(INITIAL_WIDTH, INITIAL_HEIGHT)
    window.title = "PGRF3 - horcimi1"

    val animator = FPSAnimator(window, FPS, true).also { it.start() }
    window.addWindowListener(object : WindowAdapter() {
        override fun windowDestroyNotify(e: WindowEvent) {
            thread {
                if (animator.isStarted) animator.stop()
                System.exit(0)
            }
        }
    })

    window.isVisible = true

}